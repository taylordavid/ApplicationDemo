//
//  ViewController.m
//  ApplicationDemo
//
//  Created by 解晓东 on 15/11/17.
//  Copyright (c) 2015年 sasa. All rights reserved.
//

#import "ViewController.h"
#import "SAAppInfo.h"
#import "SAAppView.h"

#define kAppViewW 80
#define kAppViewH 90
#define kColCount 3
#define kStartY 20

@interface ViewController ()

@property (nonatomic, strong) NSArray *appList;

@end

@implementation ViewController

- (NSArray *)appList {
    if (_appList == nil) {
                
        // 将临时数组为属性赋值
        _appList = [SAAppInfo appInfoList];
    }
    
    return _appList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 搭建九宫格
    
    CGFloat marginX = (self.view.bounds.size.width - kColCount * kAppViewW) / (kColCount + 1);
    CGFloat marginY = 20;
    
    for (int i = 0; i < self.appList.count; ++i) {
        int row = i / kColCount;
        int col = i % kColCount;
        
        CGFloat x = marginX + col * (marginX + kAppViewW);
        CGFloat y = kStartY + marginY + row * (marginY + kAppViewH);
        
        // 实现内部细节
        // 图片
        SAAppView *appView = [[[NSBundle mainBundle] loadNibNamed:@"SAAppView" owner:nil options:nil] firstObject];
        appView.frame = CGRectMake(x, y, kAppViewW, kAppViewH);
        NSLog(@"%@", appView);
        
        [self.view addSubview:appView];
        
        //UIImageView *icon = appView.subviews[0];
        
        SAAppInfo *appInfo = self.appList[i];
        UIImageView *icon = appView.iconImage;
       
        // 设置图像
        icon.image = appInfo.image;
        
        // 应用程序名
        // CGRectGetMaxY(frame) = frame.origin.y + frame.size.height
        //UILabel *label = appView.subviews[2];        //label.backgroundColor = [UIColor blueColor];
        UILabel *label = appView.label;
        
        label.text = appInfo.name;
        
        // 下载按钮
        // UIButton *button = appView.subviews[1];
        UIButton *button = appView.button;
        button.tag = i;
      
        // 给按钮添加监听
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)click:(UIButton *)button {
    // 禁用按钮
    button.enabled = NO;
    // 添加一个UILabel
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, 400, 160, 40)];
    label.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
    
    SAAppInfo *appInfo = self.appList[button.tag];
    
    label.text = appInfo.name;
    label.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:label];
    // 动画效果
    // 初始透明度，完全透明
    // 需要在动画结束之后删除Label
    
    label.alpha = 0.0;
    
    [UIView animateWithDuration:1.0f animations:^{
        // 要修改得动画属性
        label.alpha = 1.0;
    } completion:^(BOOL finished) {
        // 动画完成后所做的操作
        [UIView animateWithDuration:1.0f animations:^{
            label.alpha = 0.0;
        } completion:^(BOOL finished) {
            [label removeFromSuperview]; // 从视图上删除
        }];
    }];
    
    // 首尾式动画，不容易监听动画完成时间，而且不容易实现动画嵌套
    // 首尾式动画，修改对象的属性，frame，bounds，alpha
//    label.alpha = 0.0;
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1.0f];
//    label.alpha = 1.0;
//    [UIView commitAnimations];
}

@end
