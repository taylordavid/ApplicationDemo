//
//  SAAppInfo.h
//  ApplicationDemo
//
//  Created by 解晓东 on 15/11/17.
//  Copyright (c) 2015年 sasa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SAAppInfo : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon;

@property (nonatomic, strong, readonly) UIImage *image;

// 使用字典实例化模型
- (instancetype)initWithDict:(NSDictionary *)dict;
// 类方法可以快速实例化一个对象
+ (instancetype)appInfoWithDict:(NSDictionary *)dict;

+ (NSArray *)appInfoList;
@end
