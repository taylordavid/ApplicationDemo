//
//  SAAppView.h
//  ApplicationDemo
//
//  Created by 解晓东 on 15/11/17.
//  Copyright (c) 2015年 sasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAAppView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
