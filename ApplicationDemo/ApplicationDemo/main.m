//
//  main.m
//  ApplicationDemo
//
//  Created by 解晓东 on 15/11/17.
//  Copyright (c) 2015年 sasa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
