//
//  SAAppInfo.m
//  ApplicationDemo
//
//  Created by 解晓东 on 15/11/17.
//  Copyright (c) 2015年 sasa. All rights reserved.
//

#import "SAAppInfo.h"

@implementation SAAppInfo

@synthesize image = _image;

- (UIImage *)image {
    if (_image == nil) {
        _image = [UIImage imageNamed:self.icon];
    }
    
    return _image;
}

- (instancetype)initWithDict:(NSDictionary *)dict {
    // self 是对象
    self = [super init];
    
    if (self) {
        // 使用KVC
        // [self setValue:dict[@"name"] forKey:@"name"];
        // [self setValue:dict[@"icon"] forKey:@"icon"];
        // 本质上就是调用以上两句代码
        // 1.plist中得键值名称必须与模型中得属性一致
        // 2.模型中的属性可以不全部出现在plist中
        [self setValuesForKeysWithDictionary:dict];
    }
    
    return self;
}

+ (instancetype)appInfoWithDict:(NSDictionary *)dict {
    // self 是class
    return [[self alloc] initWithDict:dict];
}

+ (NSArray *)appInfoList {
    NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"app.plist" ofType:nil]];
    
    // 创建一个临时数组
    NSMutableArray *arrayM = [NSMutableArray array];
    // 遍历数组，依次转换模型
    for (NSDictionary *dict in array) {
        // SAAppInfo *appInfo = [[SAAppInfo alloc] initWithDict:dict];
        SAAppInfo *appInfo = [SAAppInfo appInfoWithDict:dict];
        
        [arrayM addObject:appInfo];
    }
    
    return arrayM;
}

@end
